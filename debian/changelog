python-idna (3.3-1+deb12u1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Tue, 02 Jul 2024 14:00:18 +0000

python-idna (3.3-1+deb12u1) bookworm; urgency=high

  * Non-maintainer upload.
  * Fix CVE-2024-3651: Specially crafted inputs to idna.encode() can consume
    significant resources, which may lead to denial of service.
    (Closes: #1069127)

 -- Guilhem Moulin <guilhem@debian.org>  Thu, 30 May 2024 14:31:22 +0200

python-idna (3.3-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 04:33:32 +0000

python-idna (3.3-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Feb 2022 12:45:05 +0100

python-idna (3.2-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add myself as uploader.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Dec 2021 15:26:29 +0100

python-idna (3.2-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Thomas Goirand ]
  * Team upload.
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Aug 2021 11:06:25 +0200

python-idna (2.10-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 00:21:44 +0000

python-idna (2.10-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.

  [ Sergio Durigan Junior ]
  * New upstream version 2.10.
  * d/tests/python3: Use '-s' instead of '-i' when invoking py3versions.
    This fixes the lintian warning
    runtime-test-file-uses-installed-python-versions.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Fri, 10 Jul 2020 14:19:14 -0400

python-idna (2.9-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 02 May 2020 13:35:55 -0400

python-idna (2.8-2) unstable; urgency=medium

  * Drop python2 support; Closes: #937826
  * mark python3-idna as Multi-Arch: foreign; Closes: #921245

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Apr 2020 20:20:22 -0400

python-idna (2.8-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Scott Kitterman ]
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 14 Mar 2020 01:40:52 -0400

python-idna (2.6-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Rename d/tests/control.autodep8 to d/tests/control.
  * Use debhelper-compat instead of debian/compat.
  * Drop PyPy support
  * Bump debhelper compat level to 12
  * Bump standards version to 4.4.0 (no changes)

 -- Ondřej Nový <onovy@debian.org>  Tue, 23 Jul 2019 16:40:00 +0200

python-idna (2.6-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 01:44:48 +0000

python-idna (2.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Bump debhelper compat level to 11
  * d/watch: Use https
  * Standards-Version is 4.1.3 now (no changes needed)
  * Enable autopkgtest-pkg-python testsuite

 -- Ondřej Nový <onovy@debian.org>  Thu, 04 Jan 2018 14:38:53 +0100

python-idna (2.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/control
    - Bump Standards-Version to 4.0.0 (no changes needed).
  * debian/copyright
    - Update upstream copyright years.

 -- Daniele Tricoli <eriol@mornie.org>  Wed, 19 Jul 2017 20:11:32 +0200

python-idna (2.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8 (no changes).

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 26 Dec 2016 16:14:46 +0200

python-idna (2.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Tristan Seligmann ]
  * New upstream release.
  * Bump Standards-Version to 3.9.7 (no changes).

 -- Tristan Seligmann <mithrandi@debian.org>  Wed, 06 Apr 2016 23:16:57 +0200

python-idna (2.0-3) unstable; urgency=medium

  * Add pypy-idna package.

 -- Tristan Seligmann <mithrandi@debian.org>  Sun, 30 Aug 2015 01:47:51 +0200

python-idna (2.0-2) unstable; urgency=medium

  * Fix broken autopkgtest scripts.

 -- Tristan Seligmann <mithrandi@debian.org>  Wed, 17 Jun 2015 08:22:12 +0200

python-idna (2.0-1) unstable; urgency=medium

  * Initial release. (closes: #756388)

 -- Tristan Seligmann <mithrandi@debian.org>  Mon, 08 Jun 2015 15:29:18 +0200
